<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    //Mpesa

    Route::post('checkSTKStatus', 'App\Http\Controllers\MpesaController@checkSTKStatus');
    Route::post('callBack', 'App\Http\Controllers\MpesaController@callBack');
    Route::post('c2bConfirm', 'App\Http\Controllers\MpesaController@c2bConfirm');
    Route::post('registerURL', 'App\Http\Controllers\MpesaController@registerURL');


    Route::post('signup', 'App\Http\Controllers\AuthController@signup');
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('forget', 'App\Http\Controllers\AuthController@forget');
    Route::post('attemptLogin', 'App\Http\Controllers\AuthController@attemptLogin');

    Route::post('initiateSubscriptionSTK', 'App\Http\Controllers\MpesaController@initiateSubscriptionSTK');
    Route::post('initiateDonationSTK', 'App\Http\Controllers\MpesaController@initiateDonationSTK');
    Route::post('checkSubscriptionStkPayment', 'App\Http\Controllers\MpesaController@checkSubscriptionStkPayment');
    Route::post('callBack', 'App\Http\Controllers\MpesaController@callBack');
    Route::post('c2bConfirm', 'App\Http\Controllers\MpesaController@c2bConfirm');
    Route::post('registerURL', 'App\Http\Controllers\MpesaController@registerURL');

  //cat
    Route::get('getCategories', 'App\Http\Controllers\CategoryController@getCategories');
    Route::get('getProducts', 'App\Http\Controllers\ProductController@getProducts');
    Route::get('getFewProducts', 'App\Http\Controllers\ProductController@getFewProducts');
    Route::get('getProductByUuid/{id}', 'App\Http\Controllers\ProductController@getProductByUuid');
    Route::get('getProductByCategory/{id}', 'App\Http\Controllers\ProductController@getProductByCategory');
    Route::get('getCurrencyConversationRate', 'App\Http\Controllers\ProductController@getCurrencyConversationRate');

    //contact
    Route::post('postSupport', 'App\Http\Controllers\SupportController@postSupport');
    Route::post('updateUserIp', 'App\Http\Controllers\LogsController@updateUserIp');
    Route::get('getIpAddresses/{id}', 'App\Http\Controllers\LogsController@getIpAddresses');





    ##############################################################################################################
     //Mpesa
     Route::post('initiateSTK', 'App\Http\Controllers\MpesaController@initiateSTK');
     //category
     Route::post('addCategory', 'App\Http\Controllers\CategoryController@addCategory');
     Route::post('editCategory', 'App\Http\Controllers\CategoryController@editCategory');
     //product
     Route::post('addProduct', 'App\Http\Controllers\ProductController@addProduct');
     Route::post('editProduct', 'App\Http\Controllers\ProductController@editProduct');
     Route::get('adminGetProducts', 'App\Http\Controllers\ProductController@adminGetProducts');
     Route::get('deleteProduct/{id}', 'App\Http\Controllers\ProductController@deleteProduct');
     //Invoice
     Route::post('checkOut', 'App\Http\Controllers\InvoiceController@checkOut');
     Route::get('getInvoiceData/{id}', 'App\Http\Controllers\InvoiceController@getInvoiceData');
     Route::get('getMyProducts', 'App\Http\Controllers\InvoiceController@getMyProducts');
     Route::get('deleteInvoice/{id}', 'App\Http\Controllers\InvoiceController@deleteInvoice');
     Route::get('getNoneDisbursed', 'App\Http\Controllers\InvoiceController@getNoneDisbursed');
     Route::get('getDisbursed', 'App\Http\Controllers\InvoiceController@getDisbursed');
     Route::get('getUnpaid', 'App\Http\Controllers\InvoiceController@getUnpaid');
     Route::post('disburseLicense', 'App\Http\Controllers\InvoiceController@disburseLicense');
     Route::post('redisburseLicense', 'App\Http\Controllers\InvoiceController@redisburseLicense');
     Route::get('downloadLicense/{id}', 'App\Http\Controllers\InvoiceController@downloadLicense');

     //paypal
     Route::get('payPalcheckout', 'App\Http\Controllers\PaypalController@payPalcheckout');
     Route::post('orderStatus/{id}', 'App\Http\Controllers\PaypalController@orderStatus');

     //Users
     Route::get('users', 'App\Http\Controllers\AuthController@users');
     Route::get('clients', 'App\Http\Controllers\AuthController@clients');
     Route::post('addUser', 'App\Http\Controllers\AuthController@addUser');
     Route::post('editUser', 'App\Http\Controllers\AuthController@editUser');
     Route::post('password', 'App\Http\Controllers\AuthController@password');
     Route::get('user', 'App\Http\Controllers\AuthController@user');
     Route::post('updateProfile', 'App\Http\Controllers\AuthController@updateProfile');

     //dashboard
     Route::get('dashboard', 'App\Http\Controllers\DashboardController@dashboard');

     //
     Route::get('mpsalogs', 'App\Http\Controllers\ReportController@mpsalogs');
     Route::get('paypallogs', 'App\Http\Controllers\ReportController@paypallogs');

     Route::get('reIvite/{id}', 'App\Http\Controllers\AuthController@reIvite');

     Route::get('getErrors', 'App\Http\Controllers\LogsController@getErrors');
     Route::get('deleteError/{id}', 'App\Http\Controllers\LogsController@deleteError');

     Route::get('getSupport', 'App\Http\Controllers\SupportController@getSupport');
     Route::post('updateSupport/{id}', 'App\Http\Controllers\SupportController@updateSupport');

     Route::get('getIpAddresses/{id}', 'App\Http\Controllers\LogsController@getIpAddresses');
     Route::post('getIpByDate', 'App\Http\Controllers\LogsController@getIpByDate');
######################################################################################################################
    Route::group([
        'middleware' => 'auth:api'
    ], function() {

    });
});

