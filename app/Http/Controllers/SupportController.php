<?php

namespace App\Http\Controllers;

use App\Models\Support;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function postSupport(Request $request){
        $support=Support::create($request->all());
        return ['status'=>true,'message'=>'Information submitted successfully'];

    }

    public function getSupport(){
        $support=Support::orderBy('status','asc')->get();
        return ['support'=>$support];
    }

    public function updateSupport(Request $request, $id){
        $support=Support::find($id);
        $support->update($request->all());
        return ['status'=>true,'message'=>'Support updated successfully'];
    }
}
