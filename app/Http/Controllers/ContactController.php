<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public  function submitContact(Request  $request){
        $contact=Contact::create($request->all());
        return ['status'=>true,'message'=>'Message submitted successfully'];
    }
}
