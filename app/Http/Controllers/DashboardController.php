<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\MpesaLogs;
use App\Models\PayPalLogs;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public  function dashboard(){
        $card=PayPalLogs::sum('paypal_amount');
        $mpesa=MpesaLogs::where('status','CONFIRMED')->sum('amount');
        $clients=User::count();
        $undisbursed=Invoice::where('status','PAID')->where('disbursement','PENDING')->count();
        $disbursed=Invoice::where('status','PAID')->where('disbursement','DISBURSED')->count();
        $pending=Invoice::where('status','PENDING')->count();
        return ['card'=>$card,'mpesa'=>$mpesa,'clients'=>$clients,'undisbursed'=>$undisbursed,'disbursed'=>$disbursed,'pending'=>$pending];
    }
}
