<?php

namespace App\Http\Controllers;

use App\Models\Errorlogs;
use App\Models\Invoice;
use App\Models\PayPalLogs;
use App\Models\Product;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Knox\AFT\AFT;
use App\Services\PayUService\Exception;

class PaypalController extends Controller
{
    public  function getAccessToken(){
        $value = Cache::remember('paypal_access_token', '28000', function () {
            $user_name=env('PAYPAL_USERNAME');
            $password=env('PAYPAL_PASSWORD');
            $url=env('PAYPAL_ACCESS_TOKEN_URL');
            $client = new Client();
            $response = $client->request('POST', $url,
                [
                    'auth' => [$user_name, $password],
                    'headers' => [
                        'Content-Type'=>'application/x-www-form-urlencoded',
                    ],
                    'form_params' => [
                        'grant_type'=>'client_credentials',
                    ],
                ]
            );
            $data=json_decode($response->getBody()->getContents());
            return $data->access_token;
        });
        return  'Bearer    '.$value;
    }

    public  function payPalcheckout()
    {
        $paypal_access_token= (new PaypalController)->getAccessToken();
        $paypal_client_token_url=env('PAYPAL_CLIENT_TOKEN_URL');
        $client_id=env('CLIENT_ID');
        return ['paypal_client_token_url'=>$paypal_client_token_url,'paypal_access_token'=>$paypal_access_token,'client_id'=>$client_id];
    }

    public  function orderStatus(Request  $request,$id){
        $ads=Invoice::find($id);
        $product=Product::find($ads->prpduct_id);
        $request['invoice_id']=$id;
        $request['user_id']=Auth::user()->id;
        $request['trans_id']=substr(md5(microtime()), 0, 10);
        $request['status'] = 'PAID';
        $request['payment_method']='CARD/PAYPAL';
        $request['payment_id']=$request->paypal_trans_id;
        $ads->update($request->all());
        $paypal_logs=PayPalLogs::create($request->all());
       
        try{
       $admins=User::where('user_type','admin')->where('role','admin')->get();
       foreach ($admins as $admin){
        if(strlen($admin->phone)==10){
            $phone=$admin->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($admin->phone,4));
        }
        $message='A client has successfully paid invoice '.$ads->invoice_no.'.Please signin to disburse thier license.@LICENSESOURCING';
        AFT::sendMessage($phone, $message);
       }
    } catch (\Exception $e) {
        $request['message']=$e->getMessage();
        Errorlogs::create($request->all());
     }
        return ['status'=>true,'message'=>'Payment successfully made.Redirecting...'];
    }


}
