<?php

namespace App\Http\Controllers;

use App\Mail\GeneralMail;
use App\Models\Hotel;
use App\Models\User;
use App\Models\Errorlogs;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Services\PayUService\Exception;
use Error;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request['pic']='/images/user1.jpg';
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $credentials = request(['email', 'password']);
        $email=User::where('email',$request->email)->count();
        if($email>0){
            return ['status'=>false,'message'=>'Email is already in use.Please login'];
        }

        $request['password']=bcrypt($request->password);
        $user=User::create($request->all());
        if(!Auth::attempt($credentials)) {
            return ['status' => false, 'message' => 'We are not able to log you in, please use login page.'];
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = \Carbon\Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);
        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];


    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message' => 'Invalid email or password'];

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);

        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];
    }

    public  function attemptLogin(Request $request){
        $email=User::where('email',$request->email)->count();
        if($email>0){
            $u=User::where('email',$request->email)->first();
            $user =User::find($u->id);
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if ($request->remember_me)
                $token->expires_at = \Carbon\Carbon::now()->addWeeks(1);
            $token->save();
            $profile=User::find($u->id);
            $profile->update($request->all());
            return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken, 'token_type' => 'Bearer', 'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()];
        }else{
            $user=User::create($request->all());
            $tokenResult =$user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
            $profile=User::find($user->id);
            return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()

            ];

        }
    }

    public  function forget(Request  $request){
        $useremail=User::where('email',$request->email)->count();
        if($useremail<=0){
            return ['status'=>false,'message'=>'The email you entered does not exist'];
        }
        $user=User::where('email',$request->email)->first();
        $code=rand(100000,999999);
        $request['password']=bcrypt($code);
        $request['password_changed']='NO';

        $d=User::where('email',$request->email)->update(['password'=>$request->password]);
        try{
        $data=[
            'subject'=>'Password reset',
            'message'=>'New password has been geenrated successfully.Please use this password to login.The password is '.$code,
            ];
       
        Mail::to($request->email)->send(new GeneralMail($data));
         } catch (\Exception $e) {
         $request['message']=$e->getMessage();
         Errorlogs::create($request->all());
         }
        return ['status'=>true,'message'=>'New password has ben sent to your email.Please enter the password'];

    }
    public function updateProfile(Request $request){
        $user=User::find(Auth::user()->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Information updated successfully'];

    }

    public function user(){
        $user=Auth::user();
        return ['user'=>$user];
    }

    public  function users(){
        $users=User::where('user_type','admin')->get();
        return ['users'=>$users];
    }
    public  function clients(){
        $users=User::orderBy('id','desc')->get();
        return ['users'=>$users];
    }
    public function reIvite(Request $request,$id){
        $user=User::find($id);
        $code=rand(100000,999999);
        $request['password']=bcrypt($code);
        $request['password_changed']='NO';
        $user->update($request->all());
       
        try{
        $data=[
            'subject'=>'Account Created',
            'message'=>'Dear '.$user->name.'.You have been signed up  on license sourcing as '.$user->role.'. Your  password is '.$code,
            ];
        Mail::to($user->email)->send(new GeneralMail($data));
      } catch (\Exception $e) {
        $request['message']=$e->getMessage();
         Errorlogs::create($request->all());
       }
        return ['status'=>true,'message'=>'Invite sent successfully.'];
    }

    public  function addUser(Request  $request){
        if(empty($request->role)){
            return ['status'=>false,'message'=>'Please specify user role'];
        }
        $request['user_type']='admin';
        $request['password_changed']='NO';
        $code=rand(100000,999999);
        $request['password']=bcrypt($code);
        $user=User::create($request->all());

        try {
        $data=[
            'subject'=>'Account Created',
            'message'=>'Dear '.$request->name.'.You have been signed up  on license sourcing as '.$request->role.'. Your  password is '.$code,
            ];
        Mail::to($request->email)->send(new GeneralMail($data));
       } catch (\Exception $e) {
        $request['message']=$e->getMessage();
         Errorlogs::create($request->all());
       }

        return ['status'=>true,'message'=>'User added successfully'];
    }

    public  function editUser(Request  $request){
        $user=User::find($request->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'User updated successfully'];
    }

    public  function password(Request  $request){
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }

        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return ['status'=>false,'message'=>'The current password is invalid'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_changed']='YES';
        $user=User::find(Auth::user()->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Password updated successfully'];
    }
}
