<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public  function mpsalogs(){

        $logs=DB::select( DB::raw("Select *,
       (SELECT invoice_no from invoices B WHERE B.id=A.invoice_id)invoice_no,
       (SELECT email from users B WHERE B.id=A.user_id)buyer_name
       from mpesa_logs A WHERE status='CONFIRMED'   ORDER BY id DESC limit 1000") );
        return ['logs'=>$logs];
    }

    public  function paypallogs(){
        $logs=DB::select( DB::raw("Select *,
       (SELECT invoice_no from invoices B WHERE B.id=A.invoice_id)invoice_no,
       (SELECT email from users B WHERE B.id=A.user_id)buyer_name
       from pay_pal_logs A    ORDER BY id DESC limit 1000") );
        return ['logs'=>$logs];
    }
}
