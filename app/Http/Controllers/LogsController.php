<?php

namespace App\Http\Controllers;

use App\Models\Errorlogs;
use App\Models\Visitor;
use Illuminate\Http\Request;
use App\Services\PayUService\Exception;
use Illuminate\Support\Facades\DB;

class LogsController extends Controller
{
    public function getErrors(){
        $errors=Errorlogs::orderBy('id','desc')->get();
        return ['errors'=>$errors];
    }
    public function deleteError($id){
        $error=Errorlogs::find($id);
        $error->delete();
        return ['status'=>true,'message'=>'Error deleted successfully'];
    }

    public function updateUserIp(Request $request){
        try{
        $ip=Visitor::create($request->all());
        } catch (\Exception $e) {
        }
        return ['status'=>true,'message'=>'Success'];
    }

    public function getIpAddresses($id){
        $ips=Visitor::orderBy('id','desc')->where('date_time',$id)->get();
        return ['ips'=>$ips];
    }

    public function getIpByDate(Request $request){
        $ips = DB::select(DB::raw("SELECT * FROM visitors WHERE DATE(created_at) BETWEEN '$request->date_from' AND '$request->date_to' ORDER BY id DESC"));
        
        return ['ips'=>$ips];
    }
}
