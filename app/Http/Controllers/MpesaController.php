<?php

namespace App\Http\Controllers;

use App\Models\Errorlogs;
use App\Models\Invoice;
use App\Models\MpesaLogs;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\In;
use MPESA;
use App\Models\User;
use Illuminate\Http\Request;
use Knox\AFT\AFT;

class MpesaController extends Controller
{
    public  function initiateSTK(Request $request){
        $request->validate([
            'phone' => 'required',
            'amount' => 'required',
            'account' => 'required',
        ]);
        try {
            $phone_captured=str_replace(' ', '', $request->phone);
            if(strlen($phone_captured)==10){
                $phone=str_replace(' ','','254'.substr($phone_captured,1));
            }else{
                $phone=str_replace(' ','','254'.substr($phone_captured,4));
            }
            
            $mpesa = MPESA::stkPush((int)$phone,(int)$request->amount,$request->account);

            $request['system_ref']='LS'.mt_rand(10000,99999);
            $request['MerchantRequestID']=$mpesa->MerchantRequestID;
            $request['CheckoutRequestID']=$mpesa->CheckoutRequestID;
            $request['amount']=$request->amount;
            $request['phone']=$phone;
            $request['account']=$request->account;
            $request['ResponseCode']=$mpesa->ResponseCode;
            $request['ResponseDescription']=$mpesa->ResponseDescription;
            $request['invoice_id']='NA';
            $request['user_id']=$phone;
            $data=MpesaLogs::create($request->all());

            return ['status'=>true,'message'=>'STK pushed initiated, please enter PIN','id'=>$data->id];

        } catch (\Exception $e) {
            return ['status'=>false,'ResponseMessage'=>$e];
        }
    }
    public  function checkSTKStatus(Request $request){
        $info=MpesaLogs::find($request->id);
        if($info->status=='CONFIRMED'){
            return ['status'=>true,'mpesa_ref'=>$info->mpesa_ref,'callback'=>true];
        }else if($info->status=='CANCELLED'){
            return ['status'=>true,'message'=>'User cancelled payment.Please wait.....','callback'=>false];
        }
        else{
            return ['status'=>false];
        }
    }

    public  function callBack(Request $request){
        try {
            Log::info('Calling back STK -------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>');
            $response = $request->all();
            Log::info($response['Body']['stkCallback']);
            $request['ResultCode'] = $response['Body']['stkCallback']['ResultCode'];
            $request['ResultDesc'] = $response['Body']['stkCallback']['ResultDesc'];
            $request['MerchantRequestID'] = $response['Body']['stkCallback']['MerchantRequestID'];
            $request['CheckoutRequestID'] = $response['Body']['stkCallback']['CheckoutRequestID'];
            if ($request->ResultCode == '0') {
                $collection = collect($response['Body']['stkCallback']['CallbackMetadata']['Item']);
                $transaction = $collection->where('Name', 'MpesaReceiptNumber')->first();
                $info=MpesaLogs::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
                $logs=MpesaLogs::find($info->id);
                $request['status']='CONFIRMED';
                $request['mpesa_ref']=$transaction['Value'];
                $request['trans_id']=$transaction['Value'];
                $request['payment_ref']=$transaction['Value'];
                $request['system_id']=$logs->system_ref;
                $request['payment_method']='M-PESA EXPRESS';
                $request['payment_id']=$transaction['Value'];
                $logs->update($request->all());
                $voic=Invoice::where('invoice_no',$logs->account)->update(['status'=>'PAID']);

                $message='Payment of License for Invoce no '.$logs->account.' is successful.Amount Ksh.'.$logs->amount;
                $data=AFT::sendMessage('0710959188,0712083128', $message,'Postman');
                
            }else{
                $info=MpesaLogs::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
                $logs=MpesaLogs::find($info->id);
                $request['status']='CANCELLED';
                $logs->update($request->all());
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            $request['message']=$e->getMessage();
            Errorlogs::create($request->all());
        }
    }
}
