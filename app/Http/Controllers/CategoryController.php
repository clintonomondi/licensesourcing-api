<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public  function addCategory(Request  $request){
        $request->validate([
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);
        $fileNameWithExt=$request->file('pic')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('pic')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore =mt_rand(1000,9999) . '_' . time() . '.' . $extesion;
        //uploadimage
        $path=$request->file('pic')->storeAs('/public/Category',$fileNameToStore);
        $request['url']=$fileNameToStore;
        $cat=Category::create($request->all());
        return ['status'=>true,'message'=>'Category created successfully'];
    }

    public  function getCategories(){
        $cat=Category::orderBy('id','desc')->get();
        return ['cat'=>$cat];
    }

    public  function editCategory(Request  $request){
        $cat=Category::find($request->id);
        if (($request->has('pic')) && !empty($request->pic)) {
            $request->validate([
                'pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
            ]);
            //get file name with extension
            $fileNameWithExt = $request->file('pic')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extesion = $request->file('pic')->getClientOriginalExtension();
            //filename to store
            $fileNameToStore = mt_rand(1000,9999) . '_' . time() . '.' . $extesion;
            //uploadimage
            $path = $request->file('pic')->storeAs('/public/Category', $fileNameToStore);
            $request['url'] = $fileNameToStore;
            Storage::delete('/public/Product/'.$cat->url);
        }
        $cat->update($request->all());
        return ['status'=>true,'message'=>'Category updated successfully'];
    }
}
