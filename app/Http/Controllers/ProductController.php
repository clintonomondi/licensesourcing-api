<?php

namespace App\Http\Controllers;

use App\Models\Product;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends Controller
{

    public  function addProduct(Request  $request){

        $request->validate([
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
            'price' => 'required|numeric',
        ]);
        if (empty($request->name)) {
            return ['status'=>false,'message'=>'Name is required'];
        }
        if (empty($request->category_id)) {
            return ['status'=>false,'message'=>'Category is required'];
        }
        $fileNameWithExt=$request->file('pic')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('pic')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore =mt_rand(1000,9999) . '_' . time() . '.' . $extesion;
        //uploadimage
        $path=$request->file('pic')->storeAs('/public/Product',$fileNameToStore);
        $request['url']=$fileNameToStore;
        $request['uuid']=Str::random(25);

        $product=Product::create($request->all());
        return ['status'=>true,'message'=>'Product created successfully'];
    }

    public  function editProduct(Request  $request){
        $data=Product::find($request->id);
       
        $request->validate([
            'price' => 'required|numeric',
        ]);
        if (empty($request->name)) {
            return ['status'=>false,'message'=>'Name is required'];
        }
        if (empty($request->category_id)) {
            return ['status'=>false,'message'=>'Category is required'];
        }
        if (($request->has('pic')) && !empty($request->pic)) {
            $request->validate([
                'pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
            ]);
            //get file name with extension
            $fileNameWithExt = $request->file('pic')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extesion = $request->file('pic')->getClientOriginalExtension();
            //filename to store
            $fileNameToStore = mt_rand(1000,9999) . '_' . time() . '.' . $extesion;
            //uploadimage
            $path = $request->file('pic')->storeAs('/public/Product', $fileNameToStore);
            $request['url'] = $fileNameToStore;
            Storage::delete('/public/Product/'.$data->url);
        }
        $data->update($request->all());
        return ['status'=>true,'message'=>'Product updated successfully'];
    }
    public function deleteProduct($id){
        $p=Product::find($id);
        Storage::delete('/public/Product/'.$p->url);
        $p->delete();
        return ['status'=>true,'message'=>'Product deleted successfully'];
    }
    public  function adminGetProducts(){
        $products=DB::select( DB::raw("SELECT *,
       (SELECT name from categories B WHERE B.id=A.category_id)category
       FROM products A order by id DESC ") );
        return ['products'=>$products];
    }

    public  function getProducts(){
        $products=DB::select( DB::raw("SELECT name,price,url,id,uuid FROM products order by rand()") );
        return ['products'=>$products];
    }
    public  function getFewProducts(){
        $products=DB::select( DB::raw("SELECT name,price,id,url,uuid FROM products order by rand() limit 8") );
        return ['products'=>$products];
    }

    public  function getProductByUuid($id){
        $product=Product::where('uuid',$id)->first();
        return ['product'=>$product];
    }



    public  function getCurrencyConversationRate(){
        $value = Cache::remember('currency_conversion_rate', '28000', function () {
        $url=env('CURRENCY_URL');
        $client = new Client();
        $response = $client->request('GET', $url,
            [
                'headers' => [
                    'Content-Type'=>'application/json',
                ],
            ]
        );
        $data=json_decode($response->getBody()->getContents());
        return $data;
        });
        return  $value;
    }

    public  function getProductByCategory($id){
        $products=DB::select( DB::raw("SELECT name,price,url,id,uuid FROM products WHERE category_id='$id' order by rand() ") );
        return ['products'=>$products];
    }

}
