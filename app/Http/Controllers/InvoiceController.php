<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Product;
use App\Models\Productkey;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\In;
use App\Mail\License;
use App\Models\Errorlogs;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller
{
    public  function checkOut(Request  $request){
        $request['invoice_no']=strtoupper('LS'.mt_rand(10000000,99999999));
        $invoice=Invoice::create($request->all());
        return ['status'=>true,'message'=>'Invoice generated successfully','invoice_id'=>$invoice->id];
    }

    public function getInvoiceData($id){
        $invoice=Invoice::find($id);
        $product=Product::find($invoice->product_id);
        return ['invoice'=>$invoice,'product'=>$product];
    }

    public  function getMyProducts(){
        $id=Auth::user()->id;
        $invoice=DB::select( DB::raw("Select *,
       (SELECT name from products B WHERE B.id=A.product_id)product_name,
       (SELECT mpesa_ref from mpesa_logs B WHERE B.invoice_id=A.id)ref,
       (SELECT payment_method from mpesa_logs B WHERE B.invoice_id=A.id)method
       from invoices A WHERE user_id='$id' ORDER BY id DESC ") );
        return ['invoices'=>$invoice];
    }

    public  function deleteInvoice($id){
        $inv=Invoice::find($id);
        $inv->delete();
        return ['status'=>true,'message'=>'Invoice deleted successfully'];
    }

    public  function getNoneDisbursed(){
        $invoice=DB::select( DB::raw("Select *,
       (SELECT name from products B WHERE B.id=A.product_id)product_name,
       (SELECT mpesa_ref from mpesa_logs B WHERE B.invoice_id=A.id)ref,
       (SELECT payment_method from mpesa_logs B WHERE B.invoice_id=A.id)method,
       (SELECT email from users B WHERE B.id=A.user_id)buyer,
       (SELECT name from users B WHERE B.id=A.user_id)buyer_name
       from invoices A WHERE status='PAID' AND disbursement='PENDING'  ORDER BY id DESC ") );
        return ['invoices'=>$invoice];
    }

    public  function getDisbursed(){
        $invoice=DB::select( DB::raw("Select *,
       (SELECT name from products B WHERE B.id=A.product_id)product_name,
       (SELECT mpesa_ref from mpesa_logs B WHERE B.invoice_id=A.id)ref,
       (SELECT payment_method from mpesa_logs B WHERE B.invoice_id=A.id)method,
       (SELECT email from users B WHERE B.id=A.user_id)buyer,
       (SELECT name from users B WHERE B.id=A.user_id)buyer_name,
       (SELECT license from productkeys B WHERE B.invoice_id=A.id LIMIT 1)license
       from invoices A WHERE status='PAID' AND disbursement='DISBURSED'  ORDER BY id DESC ") );
        return ['invoices'=>$invoice];
    }

    public  function getUnpaid(){
        $invoice=DB::select( DB::raw("Select *,
       (SELECT name from products B WHERE B.id=A.product_id)product_name,
       (SELECT mpesa_ref from mpesa_logs B WHERE B.invoice_id=A.id)ref,
       (SELECT payment_method from mpesa_logs B WHERE B.invoice_id=A.id)method,
       (SELECT email from users B WHERE B.id=A.user_id)buyer,
       (SELECT name from users B WHERE B.id=A.user_id)buyer_name
       from invoices A WHERE status='PENDING'   ORDER BY id DESC ") );
        return ['invoices'=>$invoice];
    }

    public  function disburseLicense(Request  $request){
        try {
        if(empty($request->license)){
            return ['status'=>false,'message'=>'License is required'];
        }   
        $invoice=Invoice::find($request->id);
        $product=Product::find($invoice->product_id);
        $request['disbursement']='DISBURSED';
        $invoice->update($request->all());
        $request['invoice_id']=$request->id;
        $request['created_by']=Auth::user()->id;
        $request['updated_by']=Auth::user()->id;
        $key=Productkey::create($request->all());
      
        $data=[
            'subject'=>'License Delivery',
            'message'=>$request->license,
            'url'=>'https://api.licensesourcing.com/storage/Product/'.$product->url,
        ];
        
        Mail::to($request->email)->send(new License($data));
        } catch (\Exception $e) {
            $request['message']=$e->getMessage();
            Errorlogs::create($request->all());
        }

        return ['status'=>true,'message'=>'License disbursed successfully'];
    }

    public function redisburseLicense(Request $request){
        try {
            if(empty($request->license)){
                return ['status'=>false,'message'=>'License is required'];
            }   
            $invoice=Invoice::find($request->id);
            $product=Product::find($invoice->product_id);
            $request['disbursement']='DISBURSED';
            $invoice->update($request->all());
            $request['invoice_id']=$request->id;
            $request['updated_by']=Auth::user()->id;
            $key=Productkey::where('invoice_id',$request->invoice_id)->update(['license'=>$request->license]);
          
            $data=[
                'subject'=>'License Delivery',
                'message'=>$request->license,
                'url'=>'https://api.licensesourcing.com/storage/Product/'.$product->url,
            ];
            
            Mail::to($request->email)->send(new License($data));
            } catch (\Exception $e) {
                $request['message']=$e->getMessage();
                Errorlogs::create($request->all());
            }
    
            return ['status'=>true,'message'=>'License disbursed successfully'];
    }

    public function downloadLicense(Request $request, $id){
        try {  
            $invoice=Invoice::find($id);
            $product=Product::find($invoice->product_id);
            $key=Productkey::where('invoice_id',$invoice->id)->first();
          
            $data=[
                'subject'=>'License Delivery',
                'message'=>$key->license,
                'url'=>'https://api.licensesourcing.com/storage/Product/'.$product->url,
            ];
            
            Mail::to(Auth::user()->email)->send(new License($data));
            } catch (\Exception $e) {
                $request['message']=$e->getMessage();
                Errorlogs::create($request->all());
            }
    
            return ['status'=>true,'message'=>'Please check your email '.Auth::user()->email.' for the licence.'];
    }
}
